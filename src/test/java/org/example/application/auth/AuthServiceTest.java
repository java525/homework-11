package org.example.application.auth;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class AuthServiceTest {

    public static final String LOGIN = "test1";
    public static final String PASSWORD = "0000";
    public static final User TEST_USER = new User(LOGIN, PASSWORD);
    public static final User INCORRECT_TEST_USER = new User(LOGIN, "PASSWORD");

    @Mock
    private UserStorage userStorage;

    @InjectMocks
    private AuthService authService;

    @Test
    void shouldLogin() {
        when(userStorage.findByLogin(LOGIN)).thenReturn(Optional.of(TEST_USER));

        assertFalse(authService.isAuth());

        authService.login(TEST_USER);

        assertTrue(authService.isAuth());
        assertEquals(authService.current().get(), TEST_USER);
    }

    @Test
    void shouldNotLoginIfUserWasNotFound() {
        when(userStorage.findByLogin(LOGIN)).thenReturn(Optional.empty());

        assertFalse(authService.isAuth());

        assertThrows(IllegalArgumentException.class,
                () -> authService.login(TEST_USER));

        assertFalse(authService.isAuth());
    }

    @Test
    void shouldNotLoginIfPasswordIsIncorrect() {
        when(userStorage.findByLogin(LOGIN)).thenReturn(Optional.of(TEST_USER));

        assertFalse(authService.isAuth());

        assertThrows(IllegalArgumentException.class, () -> authService.login(INCORRECT_TEST_USER));

        assertFalse(authService.isAuth());
    }

    @Test
    void shouldRegister() {
        when(userStorage.findByLogin(LOGIN)).thenReturn(Optional.empty());

        authService.register(TEST_USER);

        verify(userStorage).save(TEST_USER);
    }

    @Test
    void shouldNotRegisterIfUserExists() {
        when(userStorage.findByLogin(LOGIN)).thenReturn(Optional.of(TEST_USER));

        assertThrows(IllegalArgumentException.class,
                () -> authService.register(TEST_USER));

        verify(userStorage, never()).save(any());
    }

    @Test
    void shouldNotRegisterIfLoginOrPasswordEmpty() {

        User user = new User(null, null);

        assertThrows(IllegalArgumentException.class,
                () -> authService.register(user));

        verify(userStorage, never()).findByLogin(any());


        verify(userStorage, never()).save(any());
    }
}