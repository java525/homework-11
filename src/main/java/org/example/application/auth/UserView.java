package org.example.application.auth;

import lombok.RequiredArgsConstructor;
import org.example.application.infrastructure.ui.ViewHelper;
import org.example.application.ticket.Ticket;
import org.example.application.ticket.TicketStatus;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.Locale;
import java.util.UUID;

@RequiredArgsConstructor
public class UserView {

    private final ViewHelper viewHelper;

    public User readUser() {
        String login = viewHelper.readString("Enter login: ");
        String password = viewHelper.readString("Enter password: ");

        return new User(login, password);
    }

    public String readId() {
        String value = viewHelper.readString("Enter id: ");
        return value;
    }

    public String readComment() {
        String value = viewHelper.readString("Enter comment: ");
        return value;
    }

    public String readDescription() {
        String value = viewHelper.readString("Enter description: ");
        return value;
    }

    public Ticket readTicket(User currentUser) {
        String description = readDescription();
        String comment = readComment();
        String shortId = UUID.randomUUID().toString().replace("-","").substring(0,8);

        return new Ticket(shortId, currentUser.getLogin(), description,null, comment, TicketStatus.NEW);
    }

    public void showError(String error) {
        viewHelper.showError(error);
    }
}
