package org.example.application.auth.storage;

import lombok.RequiredArgsConstructor;
import org.example.application.auth.User;
import org.example.application.auth.UserStorage;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RequiredArgsConstructor
public class TextFileUserStorage implements UserStorage {
    private final String filePath;

    @Override
    public Stream<User> getAll() {
        try {
            return Files.lines(Path.of(filePath))
                    .map(this::extractUser);
        } catch (IOException e) {
            return Stream.empty();
        }
    }

    private User extractUser(String s) {
        String[] parts = s.split("\\s\\|\\s");
        return new User(parts[0], parts[1]);
    }

    @Override
    public Optional<User> findByLogin(String login) {
        return getAll()
                .filter(u -> u.getLogin().equals(login))
                .findFirst();
    }

    @Override
    public void save(User user) {
        try {
            Files.write(Path.of(filePath), toLine(user), StandardOpenOption.WRITE,
                    StandardOpenOption.CREATE, StandardOpenOption.APPEND);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Iterable<String> toLine(User user) {
        return List.of(user.getLogin() + " | " + user.getPassword());
    }
}
