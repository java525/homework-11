package org.example.application.auth.storage;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.example.application.auth.User;
import org.example.application.auth.UserStorage;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@RequiredArgsConstructor
public class CombinedUserStorage implements UserStorage {
    private final String filePath;
    private final ObjectMapper objectMapper;

    @Override
    public Stream<User> getAll() {
        try {
            return Files.lines(Path.of(filePath))
                    .map(s -> {
                        try {
                            return objectMapper.readValue(s, User.class);
                        } catch (JsonProcessingException e) {
                            throw new RuntimeException(e);
                        }
                    });
        } catch (IOException e) {
            return Stream.empty();
        }
    }

    @Override
    public Optional<User> findByLogin(String login) {
        return getAll()
                .filter(u -> u.getLogin().equals(login))
                .findFirst();
    }

    @Override
    public void save(User user) {
        try {
            Files.write(Path.of(filePath), toLine(user), StandardOpenOption.WRITE,
                    StandardOpenOption.CREATE, StandardOpenOption.APPEND);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Iterable<String> toLine(User user) {
        try {
            return List.of(objectMapper.writeValueAsString(user));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

}
