package org.example.application.auth;

import lombok.RequiredArgsConstructor;

import java.util.Optional;

@RequiredArgsConstructor
public class AuthService {
    private final UserStorage userStorage;
    private User currentUser = null;

    public void login(User user) {
        currentUser = userStorage.findByLogin(user.getLogin())
                .filter(u -> u.hasPassword(user.getPassword()))
                .orElseThrow(() -> new IllegalArgumentException("Incorrect login or password"));
    }

    public void register(User user) {

        if (isNullOrEmpty(user.getLogin()) || isNullOrEmpty(user.getPassword())) {
            throw new IllegalArgumentException("Login or password are empty");
        }

        boolean exists = userStorage.findByLogin(user.getLogin()).isPresent();

        if (exists) {
            throw new IllegalArgumentException("Such user already exists");
        }

        userStorage.save(user);
    }

    public void exit() {
        currentUser = null;
    }

    public boolean isAuth() {
        return currentUser != null;
    }

    public Optional<User> current() {
        return Optional.ofNullable(currentUser);
    }

    private boolean isNullOrEmpty(String value) {
        return value == null || value.trim().isEmpty();
    }
}