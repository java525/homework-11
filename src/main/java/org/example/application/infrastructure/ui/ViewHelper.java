package org.example.application.infrastructure.ui;

import lombok.RequiredArgsConstructor;

import java.io.BufferedReader;
import java.util.Scanner;

@RequiredArgsConstructor
public class ViewHelper {
    private final Scanner scanner;

    public String readString(String message) {
        System.out.print(message);
        return scanner.nextLine();
    };

    public Integer readInt(String message) {
        System.out.print(message);
        int value = scanner.nextInt();
        scanner.nextLine();
        return value;
    };

    public void showError(String error) {
        System.err.println(error);
    }
}
