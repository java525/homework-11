package org.example.application.infrastructure.menu;

import org.example.application.auth.AuthService;

public class SubMenuForAuthUsers extends Submenu {

    private final AuthService authService;

    public SubMenuForAuthUsers(String name, AuthService authService, Menu menu) {
        super(name, false, menu);
        this.authService = authService;
    }

    @Override
    public boolean isVisible() {
        return authService.isAuth();
    }
}
