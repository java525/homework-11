package org.example.application.infrastructure.menu;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class Submenu implements MenuItem{

    private final String name;
    private final boolean visible;
    private final Menu menu;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void run() {
        menu.run();

    }

    @Override
    public boolean isVisible() {
        return visible;
    }
}
