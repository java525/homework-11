package org.example.application.infrastructure.menu;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.stream.Collectors;

@AllArgsConstructor
public class Menu {
    private final List<MenuItem> itemList;
    private Scanner scanner;

    public void run() {
        while (true) {
            show();
            Optional<MenuItem> item = getChosed();
            if (item.isPresent()) {
                item.get().run();
                if (item.get().isFinal()) break;
            } else {
                System.out.println("Incorrect choice, pls try again.");
            }
        }
    }

    private Optional<MenuItem> getChosed() {
        int choice = readChoice();
        List<MenuItem> visibleItems = getVisible();

        if (choice < 0 || choice >= visibleItems.size()) {
            return Optional.empty();
        }
        return Optional.of(visibleItems.get(choice));
    }

    private int readChoice() {
        System.out.println("Enter your choice: ");
        int choice = scanner.nextInt();
        scanner.nextLine();
        return choice - 1;
    }

    private void show() {
        List<MenuItem> visibleMenuItems = getVisible();
        for (int i = 0; i < visibleMenuItems.size(); i++) {
            System.out.printf("%d - %s\n", i + 1, visibleMenuItems.get(i).getName());
        }
    }


    private List<MenuItem> getVisible() {
        return itemList.stream().filter(MenuItem::isVisible).collect(Collectors.toList());
    }
}
