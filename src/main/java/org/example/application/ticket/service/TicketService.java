package org.example.application.ticket.service;

import lombok.RequiredArgsConstructor;
import org.example.application.ticket.Ticket;
import org.example.application.ticket.TicketStatus;
import org.example.application.ticket.storage.TicketStorage;

import java.util.Optional;

@RequiredArgsConstructor
public class TicketService {
    private final TicketStorage ticketStorage;

    public void save(Ticket ticket) {
        ticketStorage.save(ticket);
    }

    public void showAll() {
        ticketStorage.getAll().forEach(s -> System.out.println(s));
    }

    public void showReportedByMe(String login) {
        ticketStorage.getAll()
                .filter(ticket -> ticket.getReporter().equals(login))
                .forEach(s -> System.out.println(s));
    }

    public void showNewTickets() {
        ticketStorage.getAll()
                .filter(ticket -> ticket.getStatus().equals(TicketStatus.NEW))
                .forEach(s -> System.out.println(s));
    }

    public void showInProgressTickets() {
        ticketStorage.getAll()
                .filter(ticket -> ticket.getStatus().equals(TicketStatus.IN_PROGRESS))
                .forEach(s -> System.out.println(s));
    }

    public void showDoneTickets() {
        ticketStorage.getAll()
                .filter(ticket -> ticket.getStatus().equals(TicketStatus.DONE))
                .forEach(s -> System.out.println(s));
    }

    public void showAssignedToMe(String login) {
        ticketStorage.getAll()
                .filter(ticket -> ticket.getAssignee().equals(login))
                .forEach(s -> System.out.println(s));
    }

    public void changeAssignee(String id, String login) {
        Optional<Ticket> ticket = ticketStorage.findById(id);
        ticket.ifPresent(ticket1 -> ticketStorage.changeAssignee(ticket1, login));
        ticketStorage.save(ticket.get());
    }

    public void changeStatusToInProgress(String id) {
        Optional<Ticket> ticket = ticketStorage.findById(id);
        ticket.ifPresent(ticket1 -> ticketStorage.changeStatus(ticket1, TicketStatus.IN_PROGRESS));
        ticketStorage.save(ticket.get());
    }

    public void changeStatusToDone(String id) {
        Optional<Ticket> ticket = ticketStorage.findById(id);
        ticket.ifPresent(ticket1 -> ticketStorage.changeStatus(ticket1, TicketStatus.DONE));
        ticketStorage.save(ticket.get());
    }

    public void changeDescription(String id, String description) {
        Optional<Ticket> ticket = ticketStorage.findById(id);
        ticket.ifPresent(ticket1 -> ticketStorage.changeDescription(ticket1, description));
        ticketStorage.save(ticket.get());
    }

    public void changeComment(String id, String comment) {
        Optional<Ticket> ticket = ticketStorage.findById(id);
        ticket.ifPresent(ticket1 -> ticketStorage.changeComment(ticket1, comment));
        ticketStorage.save(ticket.get());
    }


    public void filter() {

    }

    public void markAsDone() {}
}
