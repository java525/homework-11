package org.example.application.ticket.storage;

import org.example.application.ticket.Ticket;
import org.example.application.ticket.TicketStatus;

import java.util.Optional;
import java.util.stream.Stream;

public interface TicketStorage {
    Stream<Ticket> getAll ();
    Optional<Ticket> findById(String id);
    void save(Ticket ticket);

    void changeAssignee(Ticket ticket, String value);

    void changeComment(Ticket ticket, String value);

    void changeStatus(Ticket ticket, TicketStatus ticketStatus);

    void changeDescription(Ticket ticket, String value);
}
