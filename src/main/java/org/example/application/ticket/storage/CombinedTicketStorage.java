package org.example.application.ticket.storage;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.example.application.ticket.Ticket;
import org.example.application.ticket.TicketStatus;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@RequiredArgsConstructor
public class CombinedTicketStorage implements TicketStorage {
    private final String filePath;
    private final ObjectMapper objectMapper;

    public Stream<Ticket> getAll() {
        try {
            return Files.lines(Path.of(filePath))
                    .map(s -> {
                        try {
                            return objectMapper.readValue(s, Ticket.class);
                        } catch (JsonProcessingException e) {
                            throw new RuntimeException(e);
                        }
                    });
        } catch (IOException e) {
            return Stream.empty();
        }
    }

    public Optional<Ticket> findById(String id) {
        return getAll()
                .filter(t -> t.getId().equals(id))
                .findFirst();
    }

    public void changeAssignee(Ticket ticket, String value) {
        ticket.setAssignee(value);
    }

    public void changeComment(Ticket ticket, String value) {
        ticket.setComment(value);
    }

    public void changeDescription(Ticket ticket, String value) {
        ticket.setDescription(value);
    }

    public void changeStatus(Ticket ticket, TicketStatus ticketStatus) {
        ticket.setStatus(ticketStatus);
    }

    public void save(Ticket ticket) {

        Path generalPath = Path.of(filePath);

        if (findTicket(ticket) == null) {
            try {
                Files.write(generalPath, toLine(ticket), StandardOpenOption.WRITE,
                        StandardOpenOption.CREATE, StandardOpenOption.APPEND);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            try {
                Path tempFile = Files.createTempFile(null, null);

                getAll().map(t -> {
                    if (t.getId().equals(ticket.getId())) {
                        return ticket;
                    } else {
                        return t;
                    }
                }).map(this::toLine).forEach(ticketLine -> {
                    try {
                        Files.write(tempFile, ticketLine, StandardOpenOption.APPEND);
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                });

                Files.delete(generalPath);
                Files.move(tempFile, generalPath);

            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private Iterable<String> toLine(Ticket ticket) {
        try {
            return List.of(objectMapper.writeValueAsString(ticket));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    private Ticket findTicket(Ticket ticket) {
        Ticket oldTicket = getAll().filter(t -> t.getId().equals(ticket.getId()))
                .findAny()
                .orElse(null);

        return oldTicket;
    }

}
