package org.example.application.ticket;

public enum TicketStatus {
    NEW,
    IN_PROGRESS,
    DONE
}
