package org.example.application.ticket;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Ticket {
    public void setId(String id) {
        this.id = id;
    }

    public void setReporter(String reporter) {
        this.reporter = reporter;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setStatus(TicketStatus status) {
        this.status = status;
    }

    private String id;
    private String reporter;
    private String description;
    private String assignee;
    private String comment;
    private TicketStatus status;

    @Override
    public String toString() {
        return "ID: " + id + ", Reporter: " + reporter +
                ", Description: " + description +
                ", Assignee: " + assignee +
                ", Comment: " + comment +
                ", Status: " + status;
    }
}

