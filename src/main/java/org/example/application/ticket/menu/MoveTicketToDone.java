package org.example.application.ticket.menu;

import lombok.RequiredArgsConstructor;
import org.example.application.auth.AuthService;
import org.example.application.auth.UserView;
import org.example.application.infrastructure.menu.MenuItem;
import org.example.application.ticket.service.TicketService;

@RequiredArgsConstructor
public class MoveTicketToDone implements MenuItem {
    private final AuthService authService;
    private final TicketService ticketService;
    private final UserView userView;

    @Override
    public String getName() {
        return "Change status for ticket to DONE";
    }

    @Override
    public void run() {
        String id = userView.readId();
        ticketService.changeStatusToDone(id);
    }

    @Override
    public boolean isVisible() {
        return authService.isAuth();
    }
}
