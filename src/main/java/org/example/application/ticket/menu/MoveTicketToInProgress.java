package org.example.application.ticket.menu;

import lombok.RequiredArgsConstructor;
import org.example.application.auth.AuthService;
import org.example.application.auth.UserView;
import org.example.application.infrastructure.menu.MenuItem;
import org.example.application.ticket.service.TicketService;

@RequiredArgsConstructor
public class MoveTicketToInProgress implements MenuItem {
    private final AuthService authService;
    private final TicketService ticketService;
    private final UserView userView;

    @Override
    public String getName() {
        return "Change status for ticket to IN PROGRESS";
    }

    @Override
    public void run() {
        String id = userView.readId();
        ticketService.changeStatusToInProgress(id);
    }

    @Override
    public boolean isVisible() {
        return authService.isAuth();
    }
}
