package org.example.application.ticket.menu;

import lombok.RequiredArgsConstructor;
import org.example.application.auth.AuthService;
import org.example.application.infrastructure.menu.MenuItem;
import org.example.application.ticket.service.TicketService;

@RequiredArgsConstructor
public class ShowTicketsAssignedToMe implements MenuItem {
    private final AuthService authService;
    private final TicketService ticketService;

    @Override
    public String getName() {
        return "Show tickets assigned to me";
    }

    @Override
    public void run() {
        try {
            ticketService.showAssignedToMe(authService.current().get().getLogin());
        } catch (NullPointerException e) {
            System.out.println("There are tickets assigned to you yet");
        }
    }

    @Override
    public boolean isVisible() {
        return authService.isAuth();
    }
}
