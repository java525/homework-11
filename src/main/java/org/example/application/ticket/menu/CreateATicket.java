package org.example.application.ticket.menu;

import lombok.RequiredArgsConstructor;
import org.example.application.auth.AuthService;
import org.example.application.auth.UserView;
import org.example.application.infrastructure.menu.MenuItem;
import org.example.application.ticket.Ticket;
import org.example.application.ticket.service.TicketService;

@RequiredArgsConstructor
public class CreateATicket implements MenuItem {
    private final AuthService authService;
    private final UserView userView;
    private final TicketService ticketService;

    @Override
    public String getName() {
        return "Create ticket";
    }

    @Override
    public void run() {
        Ticket ticket = userView.readTicket(authService.current().get());
        try {
            ticketService.save(ticket);
        } catch (IllegalArgumentException e) {
            userView.showError(e.getMessage());
        }
    }

    @Override
    public boolean isVisible() {
        return authService.isAuth();
    }
}
