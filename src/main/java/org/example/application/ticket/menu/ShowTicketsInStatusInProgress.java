package org.example.application.ticket.menu;

import lombok.RequiredArgsConstructor;
import org.example.application.auth.AuthService;
import org.example.application.infrastructure.menu.MenuItem;
import org.example.application.ticket.service.TicketService;

@RequiredArgsConstructor
public class ShowTicketsInStatusInProgress implements MenuItem {
    private final AuthService authService;
    private final TicketService ticketService;

    @Override
    public String getName() {
        return "Show tickets in progress";
    }

    @Override
    public void run() {
        try {
            ticketService.showInProgressTickets();
        } catch (NullPointerException e) {
            System.out.println("There are no tickets in status IN PROGRESS");
        }
    }

    @Override
    public boolean isVisible() {
        return authService.isAuth();
    }
}
