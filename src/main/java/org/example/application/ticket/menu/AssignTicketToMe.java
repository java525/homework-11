package org.example.application.ticket.menu;

import lombok.RequiredArgsConstructor;
import org.example.application.auth.AuthService;
import org.example.application.auth.UserView;
import org.example.application.infrastructure.menu.MenuItem;
import org.example.application.ticket.service.TicketService;
import org.example.application.ticket.storage.TicketStorage;

@RequiredArgsConstructor
public class AssignTicketToMe implements MenuItem {
    private final AuthService authService;
    private final TicketService ticketService;
    private final UserView userView;
    private final TicketStorage ticketStorage;

    @Override
    public String getName() {
        return "Assign ticket to me";
    }

    @Override
    public void run() {
        String id = userView.readId();
        String login = String.valueOf(authService.current().get().getLogin());
        ticketService.changeAssignee(id, login);
    }

    @Override
    public boolean isVisible() {
        return authService.isAuth();
    }
}
