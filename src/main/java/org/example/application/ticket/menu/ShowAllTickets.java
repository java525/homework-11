package org.example.application.ticket.menu;

import lombok.RequiredArgsConstructor;
import org.example.application.auth.AuthService;
import org.example.application.infrastructure.menu.MenuItem;
import org.example.application.ticket.service.TicketService;

@RequiredArgsConstructor
public class ShowAllTickets implements MenuItem {
    private final AuthService authService;
    private final TicketService ticketService;

    @Override
    public String getName() {
        return "Show all tickets";
    }

    @Override
    public void run() {
        ticketService.showAll();
    }

    @Override
    public boolean isVisible() {
        return authService.isAuth();
    }
}
