package org.example.application;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.application.auth.AuthService;
import org.example.application.auth.UserStorage;
import org.example.application.auth.UserView;
import org.example.application.auth.menu.LoginMenuItem;
import org.example.application.auth.menu.LogoutMenuItem;
import org.example.application.auth.menu.RegisterMenuItem;
import org.example.application.auth.storage.CombinedUserStorage;
import org.example.application.infrastructure.menu.*;
import org.example.application.infrastructure.ui.ViewHelper;
import org.example.application.ticket.menu.*;
import org.example.application.ticket.service.TicketService;
import org.example.application.ticket.storage.CombinedTicketStorage;
import org.example.application.ticket.storage.TicketStorage;

import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ObjectMapper objectMapper = new ObjectMapper();
        ViewHelper viewHelper = new ViewHelper(scanner);

        UserView userView = new UserView(viewHelper);
        //UserStorage userStorage = new InMemoryUserStorage();
        //UserStorage userStorage = new JsonFileUserStorage("users.json", objectMapper);
        //UserStorage userStorage = new TextFileUserStorage("users.txt");
        UserStorage userStorage = new CombinedUserStorage("jsonusers.txt", objectMapper);
        TicketStorage ticketStorage = new CombinedTicketStorage("newjsontickets.txt", objectMapper);
        AuthService authService = new AuthService(userStorage);
        TicketService ticketService = new TicketService(ticketStorage);
        List<MenuItem> items = List.of(
                new LoginMenuItem(authService, userView),
                new RegisterMenuItem(authService, userView),
                new CreateATicket(authService, userView, ticketService),
                new ShowAllTickets(authService, ticketService),
                new SubMenuForAuthUsers("Edit ticket", authService, new Menu(
                        List.of(
                                new ShowAllTickets(authService, ticketService),
                                new AssignTicketToMe(authService, ticketService, userView, ticketStorage),
                                new MoveTicketToInProgress(authService, ticketService, userView),
                                new MoveTicketToDone(authService, ticketService, userView),
                                new ChangeTicketDescription(authService, ticketService, userView),
                                new ChangeTicketComment(authService, ticketService, userView),
                                new ExitMenuItem()), scanner)),
                new SubMenuForAuthUsers("Filter tickets", authService, new Menu(
                        List.of(
                                new ShowAllTickets(authService, ticketService),
                                new ShowTicketsReportedByMe(authService, ticketService),
                                new ShowTicketsAssignedToMe(authService, ticketService),
                                new ShowTicketsInStatusNew(authService, ticketService),
                                new ShowTicketsInStatusInProgress(authService, ticketService),
                                new ShowTicketsInStatusDone(authService, ticketService),
                                new ExitMenuItem()), scanner)),
                new LogoutMenuItem(authService),
                new ExitMenuItem()
        );

        Menu menu = new Menu(items, scanner);
        menu.run();

    }
}